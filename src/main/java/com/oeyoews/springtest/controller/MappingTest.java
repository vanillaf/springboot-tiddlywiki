package com.oeyoews.springtest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
public class MappingTest {
  @RequestMapping("/")
  public String Index() {
    System.out.println("Coming ... ");
    String index = "pages/index.html";
    return index;
  }

}
